#ifndef OPENXT_H
#define OPENXT_H

// 
// OpenXT Framebuffer: PV framebuffer driver for the OpenXT display handler
//
// Copyright (C) 2016 Assured Information Security, Inc. All rights reserved.
// Author: Kyle J. Temkin  <temkink@ainfosec.com>
// Author: Rian Quinn      <quinnr@ainfosec.com>
//

/******************************************************************************/
/* Framebuffer Properties                                                     */
/******************************************************************************/

//Specify the number of colors we'll use when emulating a less-than-true-color
//console.
static const size_t openxtfb_palette_color_count = 256;

//Specify the default width and height for a framebuffer. These should only
//be used in exceptional cases-- for example, if the Display Handler sends
//us an Add Display request without first sending a size hint.
static const size_t openxtfb_default_width_pixels = 1024;
static const size_t openxtfb_default_height_pixels = 768;

//Specify the maximum width and height we'll accept from the user. 
//This is intended more as a sanity check of entered values rather
//than a hard limit. Feel free to bump this if you ever need to,
//and please get me the monitors you're using those on.
static const uint32_t openxtfb_max_width  = 50000;
static const uint32_t openxtfb_max_height = 50000;

//We use ARGB buffers for the guests. This is the format going from left to
//right, or in other words, A is the upper most bits, and B is the lower
//most bits. TODO: Rename me! TODO: Use something from the display helper.
#define FB_BPP 32

//TODO: Get these from the display helper!
static const size_t openxtfb_cursor_width_pixels  = 64;
static const size_t openxtfb_cursor_height_pixels = 64;

/******************************************************************************/
/* Useful Macros                                                              */
/******************************************************************************/

/*
 * The following provides some debugging facilities. To enable debugging,
 * define DEBUG prior to including this header.
 */
#if defined(OPENXTFB_DEBUG) && defined(__KERNEL__)
#define openxtfb_error(...) printk(KERN_ERR "openxtfb[error]: " __VA_ARGS__)
#define openxtfb_debug(...) printk(KERN_INFO "openxtfb[debug]: " __VA_ARGS__)
#elif defined(__KERNEL__)
#define openxtfb_error(...) printk(KERN_ERR "openxtfb[error]: " __VA_ARGS__)
#define openxtfb_debug(...) pr_debug("openxtfb[debug]:" __VA_ARGS__)
#else
#define openxtfb_error(...)
#define openxtfb_debug(...)
#endif

/*
 * The following macro provides a simple way to validate a pointer, and prevent
 * execution of a function if a pointer is not valid. There is also an assert
 * version if you're looking to assert some condition.
 */
#define openxtfb_checkp(a,...) if (!(a)) {printk(KERN_ERR "openxtfb: %s failed. " \
    "%s == NULL\n", __PRETTY_FUNCTION__, #a); return __VA_ARGS__; }
#define openxtfb_assert(a,...) if (!(a)) {printk(KERN_ERR "openxtfb: %s failed. " \
    "%s == false\n", __PRETTY_FUNCTION__, #a); return __VA_ARGS__; }


/******************************************************************************/
/* IOCTL Definitions                                                          */
/******************************************************************************/

//The fbdev subsystem uses a capital letter 'F' as its ioctl base, though
//it doesn't explicitly define this anywhere.
#define FB_IOCTL_BASE 'F'

//The current fbdev core uses ioctls up to 0x20. We'll start our first
//ioctl some distance aboe that, so the fbdev core can tell the difference
//between our IOCTLs and its internal ones.
#define OPENXTFB_IOCTL_OFFSET 0x30

struct openxtfb_caps {
    //True iff the framebuffer's host has cursor support.
    uint8_t supports_cursor;
};


/**
 * Describes a cursor image to be loaded into OpenXT fb.
 */
struct openxtfb_cursor_image {

    //The cursor image, in RGBA8888 format.
    void * image;

    //The width and height of the provided image.
    uint32_t width;
    uint32_t height;
};


/** 
 * Describes an OpenXT-FB cursor.
 */ 
struct openxtfb_cursor_properties {

    //The cursor's "hot spot": that is, the location on
    //the cursor from which clicks will originate.
    uint32_t hotspot_x;
    uint32_t hotspot_y;
};

/**
 * Describes the location of an OpenXT-FB cursor.
 */ 
struct openxtfb_cursor_location {
    uint32_t x;
    uint32_t y;
};


/**
 * Describes the recommended dimensions for the current display,
 * and information about the framebuffer's physical layout.
 */
struct openxtfb_size_hint {

  //Recommended size.
  uint32_t width;
  uint32_t height;

  //For the userland driver's information:
  uint32_t max_width;
  uint32_t max_height;
  uint32_t stride;

  //Set if the recommendation was provided by
  //the display handler; clear if we're using a
  //default recommendation.
  uint8_t from_display_handler;

};

/**
 * Get capabilities: 
 * Returns a summary of the Display Handler's capabilities.
 */
#define OPENXTFB_IOCTL_GET_CAPS                  _IOR(FB_IOCTL_BASE, OPENXTFB_IOCTL_OFFSET + 1, struct openxtfb_caps)

/**
 * Loads a new cursor image into the OpenXT framebuffer.
 *
 * Does not change the cursor's visibilty; use set_cursor_visibility to
 * make the loaded cursor visible.
 */
#define OPENXTFB_IOCTL_LOAD_CURSOR_IMAGE         _IOW(FB_IOCTL_BASE, OPENXTFB_IOCTL_OFFSET + 2, struct openxtfb_cursor_image)

/**
 * Updates an OpenXT-FB cursor's properties.
 */ 
#define OPENXTFB_IOCTL_SET_CURSOR_PROPERTIES     _IOW(FB_IOCTL_BASE, OPENXTFB_IOCTL_OFFSET + 3, struct openxtfb_cursor_properties)

/**
 * Sets the visibility of the OpenXT-FB cursor.
 * Pass in a true argument to make the cursor visible; or null to make 
 * the cursor not visible.
 */ 
#define OPENXTFB_IOCTL_SET_CURSOR_VISIBLITY      _IOW(FB_IOCTL_BASE, OPENXTFB_IOCTL_OFFSET + 4, uint8_t)

/**
 * Specifies the location on the framebuffer of the OpenXT-FB cursor.
 */
#define OPENXTFB_IOCTL_MOVE_CURSOR               _IOW(FB_IOCTL_BASE, OPENXTFB_IOCTL_OFFSET + 5, struct openxtfb_cursor_location)


/**
 * Queries the OpenXT framebuffer for recommended resolution information.
 */
#define OPENXTFB_IOCTL_GET_SIZE_HINT             _IOR(FB_IOCTL_BASE, OPENXTFB_IOCTL_OFFSET + 6, struct openxtfb_size_hint)


#endif
